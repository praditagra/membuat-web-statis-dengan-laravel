<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
</head>

<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="/welcome" method="POST">
        @csrf
        <label>First Name</label><br><br>
        <input type="text" name="first_name"><br><br>
        <label>Last Name</label><br><br>
        <input type="text" name="last_name"><br><br>
        <label>Gender</label><br><br>
        <input type="radio" name="Gender">Male<br>
        <input type="radio" name="Gender">Female<br>
        <input type="radio" name="Gender">Other<br><br>
        <label>Nationality</label><br><br>
        <select name="Nationality" id="Nationality">
            <option value="Indonesian">Indonesian</option>
            <option value="English">English</option>
            <option value="Arabic">Arabic</option>
            <option value="Japanese">Japanese</option>
        </select><br><br>
        <label>Language Spoken</label><br><br>
        <input type="checkbox" value="Bahasa Indonesia">Bahasa Indonesia<br>
        <input type="checkbox" value="English">English<br>
        <input type="checkbox" value="Arabic">Arab<br>
        <input type="checkbox" value="Japan">Japan<br>
        <input type="checkbox" value="Other">Other<br>
        <label>Bio</label><br><br>
        <textarea name="Bio" cols="30" rows="10"></textarea><br><br>
        <input type="Submit" value="Sign Up">
    </form>
</body>

</html>